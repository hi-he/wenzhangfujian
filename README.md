### **\*The Repo has been moved to [GitHub](https://github.com/he-hai/PubSuppl). Changes after April 19th 2020 will not be updated on the GitLab version.***

This is a repository for supplementary scripts and notebooks of publications.

---
### 0. 0_ecoli_models
The directory contains *E. coli* genome-scale metabolic model: the core model and the most updated model *i*ML1515, 
and some notes on the models. 

### 1. 2020_formaldehyde condensation
The directory contains Jupyter notebooks of modelling _in vivo_ formaldehyde-THF
condensation ([He *et al.*, *Metabolites* 2020](https://doi.org/10.3390/metabo10020065)). 

The formaldehyde-THF spontaneous condensation reaction was added to the 
adjusted *E. coli* genome-scale metabolic model [*i*ML1515](https://doi.org/10.1038/nbt.3956). 
Flux balance analysis and [phenotypic phase plane](https://doi.org/10.1002/bit.10047) 
calculations were conducted using [COBRApy](https://doi.org/10.1186/1752-0509-7-74).


### 2. 2020_Promiscuous aldolases
The directory contains Jupyter notebooks of MDF and FBA modelling of the newly designed formaldehyde assimilation pathway, the homoserine cycle ([He *et al.*, *Metabolic Engineering* 2020](https://doi.org/10.1016/j.ymben.2020.03.002)).